# README #


### Requerimentos ###

Lombok (https://projectlombok.org/)
Maven  (https://maven.apache.org/)
Java 8 (https://java.com)

### Como rodar? ###

Rodar como aplicativo:
* Execute o arquivo main;

* Accessar http://localhost:8080

Ou rodar como aplicativo web:

* Fazer o build
* Mover o artefator gerato em taget/*.war para dentro da pasta do seu servidor de aplicação.

