package sienge.domain.service.imp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import sienge.domain.model.Charge;
import sienge.domain.model.Path;
import sienge.domain.model.Route;
import sienge.domain.model.street.PavedStreet;
import sienge.domain.model.street.Street;
import sienge.domain.model.street.UnpavedStreet;
import sienge.domain.model.vehicle.BucketTruck;
import sienge.domain.model.vehicle.Cart;
import sienge.domain.model.vehicle.ChestTruck;
import sienge.domain.model.vehicle.Vehicle;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class RouteServiceImplTest {


  @Parameterized.Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][]{
        {createCharge(8), new BucketTruck(), Arrays.asList(createPathWithPavedStreet(100)), new BigDecimal("62.70")},
        {createCharge(4), new ChestTruck(), Arrays.asList(createPathWithUnpavedStreet(60)), new BigDecimal("37.20")},
        {createCharge(12), new Cart(), Arrays.asList(createPathWithUnpavedStreet(180)), new BigDecimal("150.19")},
        {createCharge(6), new ChestTruck(), Arrays.asList(createPathWithPavedStreet(80), createPathWithUnpavedStreet(20)),
            new BigDecimal("57.60")},
        {createCharge(5), new BucketTruck(), Arrays.asList(createPathWithPavedStreet(50), createPathWithUnpavedStreet(30)),
            new BigDecimal("47.88")},
    });
  }

  private RouteServiceImpl routeService = new RouteServiceImpl();
  private Charge charge;
  private Vehicle vehicle;
  private List<Path> paths;
  private BigDecimal expectedValue;

  public RouteServiceImplTest(Charge charge, Vehicle vehicle, List<Path> paths, BigDecimal expectedValue) {
    this.charge = charge;
    this.vehicle = vehicle;
    this.paths = paths;
    this.expectedValue = expectedValue;
  }

  @Test
  public void shouldGetTotalValue() {
    Route route = new Route();
    route.setPaths(paths);
    route.setVehicle(vehicle);
    route.setCharge(charge);
    assertEquals(expectedValue, routeService.calculateTotal(route));
  }

  private static Path createPath(int distance, Street street) {
    Path path = new Path();
    path.setDistance(distance);
    path.setStreet(street);
    return path;
  }

  private static Path createPathWithUnpavedStreet(int distance) {
    return createPath(distance, new UnpavedStreet());
  }

  private static Path createPathWithPavedStreet(int distance) {
    return createPath(distance, new PavedStreet());
  }

  private static Charge createCharge(int weigth) {
    Charge charge = new Charge();
    charge.setWeightInTons(weigth);
    return charge;
  }

}