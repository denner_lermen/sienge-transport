package sienge.domain.model;

import org.junit.Test;

import java.math.BigDecimal;

import sienge.domain.model.street.PavedStreet;
import sienge.domain.model.street.UnpavedStreet;

import static org.junit.Assert.*;

public class PathTest {


  @Test
  public void whenDistanceIs100InPavedStreetThenReturnTotalValue5400() {
    Path path = new Path();
    path.setStreet(new PavedStreet());
    path.setDistance(100);
    assertEquals(new BigDecimal("54.00"), path.getValueTotal());
  }

  @Test
  public void whenDistanceIs100InUnpavedStreetThenReturnTotalValue6200() {
    Path path = new Path();
    path.setStreet(new UnpavedStreet());
    path.setDistance(100);
    assertEquals(new BigDecimal("62.00"), path.getValueTotal());
  }

  @Test
  public void whenDistanceIs60InUnpavedStreetThenReturnTotalValue3720() {
    Path path = new Path();
    path.setStreet(new UnpavedStreet());
    path.setDistance(60);
    assertEquals(new BigDecimal("37.20"), path.getValueTotal());
  }

}