package sienge.domain.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class ChargeTest {

  @Test
  public void whenWeightIs3ReturnZeroInAdditionalValue0() {
    Charge charge = new Charge();
    charge.setWeightInTons(3);
    assertEquals(charge.getAdditionalValuePerKm(), BigDecimal.ZERO);
  }

  @Test
  public void whenWeightIs5ReturnZeroInAdditionalValue0() {
    Charge charge = new Charge();
    charge.setWeightInTons(5);
    assertEquals(charge.getAdditionalValuePerKm(), BigDecimal.ZERO);
  }

  @Test
  public void whenWeightIs6ReturnAdditionalValue002() {
    Charge charge = new Charge();
    charge.setWeightInTons(6);
    assertEquals(new BigDecimal("0.02"), charge.getAdditionalValuePerKm());
  }

  @Test
  public void whenWeightIs10ReturnAdditionalValue010() {
    Charge charge = new Charge();
    charge.setWeightInTons(10);
    assertEquals(new BigDecimal("0.10"), charge.getAdditionalValuePerKm());
  }

  @Test
  public void whenWeightIs40ReturnAdditionalValue070() {
    Charge charge = new Charge();
    charge.setWeightInTons(40);
    assertEquals(new BigDecimal("0.70"), charge.getAdditionalValuePerKm());
  }

  @Test
  public void whenWeightIs77ReturnAdditionalValueEquals144() {
    Charge charge = new Charge();
    charge.setWeightInTons(77);
    assertEquals(new BigDecimal("1.44"), charge.getAdditionalValuePerKm());
  }

  @Test
  public void whenWeightIs7ReturnAdditionalValue004() {
    Charge charge = new Charge();
    charge.setWeightInTons(7);
    assertEquals(new BigDecimal("0.04"), charge.getAdditionalValuePerKm());
  }
}