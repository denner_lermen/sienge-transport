angular.module('app', [])
    .controller('IndexController',
                ['VehicleService', 'StreetService', 'RouteService',
                 function (vehicleService, streetService, routeService) {
                     var vm = this;
                     vm.entity = {};
                     vm.vehicles = [];
                     vm.streets = [];
                     vm.paths = {};

                     vm.submitForm = function (form) {
                         if (!form.$valid) {
                             console.error('invalid form');
                             return;
                         }

                         var route = {};
                         route.vehicle = vm.entity.vehicle;
                         route.charge = vm.entity.charge;
                         route.paths = [];

                         vm.streets.forEach(function (street) {
                             if (street.distance > 0) {
                                 route.paths.push({street: street, distance: street.distance});
                             }
                         });

                         routeService.calculate(route).then(function (valueTotal) {
                             vm.valueTotal = valueTotal;
                         });

                     };

                     vehicleService.findAll().then(function (vehicles) {
                         vm.vehicles = vehicles;
                     });

                     streetService.findAll().then(function (streets) {
                         vm.streets = streets;
                     });

                 }])
    .factory('VehicleService', ['$http', function ($http) {

        return {
            findAll: function () {
                return $http.get('/v1/vehicles').then(
                    function (reponse) {
                        return (reponse.data);
                    });
            }
        };
    }])
    .factory('StreetService', ['$http', function ($http) {

        return {
            findAll: function () {
                return $http.get('/v1/streets').then(
                    function (reponse) {
                        return (reponse.data);
                    });
            }
        };
    }]).factory('RouteService', ['$http', function ($http) {

    return {
        calculate: function (route) {
            console.log(route);
            return $http.post('/v1/routes/calculate', route).then(
                function (reponse) {
                    return (reponse.data);
                });
        }
    };
}]);