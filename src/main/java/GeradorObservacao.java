
import org.apache.commons.lang3.BooleanUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class GeradorObservacao {

  private static final String TEXTO_FIXO_UMA_NOTA = "Fatura da nota fiscal de simples remessa: ";
  private static final String TEXTO_FIXO_VARIAS_NOTAS = "Fatura das notas fiscais de simples remessa: ";
  private static final String PONTO_FINAL = ".";
  private static final String SEPARADOR_VIRGULA = ", ";
  private static final String SEPARADOR_E = " e ";

  public String geraObservacao(List lista) {
    if (lista.isEmpty()) {
      return "";
    }

    StringBuilder textoDeRetorno = new StringBuilder();
    textoDeRetorno.append(retornaOTextoFixo(lista));
    textoDeRetorno.append(retornaCodigos(lista));
    textoDeRetorno.append(PONTO_FINAL);
    return textoDeRetorno.toString();

  }

  private String retornaCodigos(List lista) {
    String result = lista.parallelStream().limit(lista.size() - 1).map(x -> String.valueOf(x))
        .collect(Collectors.joining(SEPARADOR_VIRGULA)).toString();

    return lista.size() > 1 ? result + SEPARADOR_E + lista.get(lista.size() - 1) : String.valueOf(lista.get(0));
  }

  private String retornaOTextoFixo(List lista) {
    if (lista.size() >= 2) {
      return TEXTO_FIXO_VARIAS_NOTAS;
    } else {
      return TEXTO_FIXO_UMA_NOTA;
    }
  }
}