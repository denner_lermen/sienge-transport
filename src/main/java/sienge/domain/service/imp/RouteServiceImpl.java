package sienge.domain.service.imp;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import lombok.extern.slf4j.Slf4j;
import sienge.domain.model.Route;
import sienge.domain.service.RouteService;

@Service
@Slf4j
public class RouteServiceImpl implements RouteService {

  @Override
  public BigDecimal calculateTotal(Route route) {
    BigDecimal total;
    BigDecimal totalPaths;
    totalPaths = route.getPaths().parallelStream().map(path -> path.getValueTotal()).reduce(BigDecimal.ZERO, BigDecimal::add);
    total = totalPaths.multiply(route.getVehicle().getFactorMultiplier());
    if (route.getCharge().isExceededWeight()) {
      int totalDistance = route.getPaths().parallelStream().mapToInt(path -> path.getDistance()).sum();
      BigDecimal totalAdditional = route.getCharge().getAdditionalValuePerKm().multiply(new BigDecimal(totalDistance));
      total = totalAdditional.add(total);
    }
    log.info("Total value: " + total);
    return total.setScale(2, RoundingMode.HALF_EVEN);
  }
}
