package sienge.domain.service;

import java.math.BigDecimal;

import sienge.domain.model.Route;

public interface RouteService {


  BigDecimal calculateTotal(Route route);
}
