package sienge.domain.model.street;

import java.math.BigDecimal;

public class UnpavedStreet extends Street {

  public static final BigDecimal PRICE_PER_KM = new BigDecimal("0.62");
  public static final String NAME = "Não-pavimentada";

  public UnpavedStreet() {
    super(NAME, PRICE_PER_KM);
  }

}
