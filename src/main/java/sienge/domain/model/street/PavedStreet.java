package sienge.domain.model.street;

import java.math.BigDecimal;

/**
 * Created by denne on 06/06/2017.
 */
public class PavedStreet extends Street {

  public static final BigDecimal PRICE_PER_KM = new BigDecimal("0.54");
  public static final String NAME = "Pavimentada";

  public PavedStreet() {
    super(NAME, PRICE_PER_KM);
  }


}
