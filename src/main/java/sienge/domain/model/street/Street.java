package sienge.domain.model.street;

import java.math.BigDecimal;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Street {

  private BigDecimal pricePerKm;
  private String name;

  public Street(){}

  public Street(String name, BigDecimal pricePerKm) {
    this.name = name;
    this.pricePerKm = pricePerKm;
  }
}
