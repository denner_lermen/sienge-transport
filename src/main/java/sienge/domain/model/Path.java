package sienge.domain.model;

import java.math.BigDecimal;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import sienge.domain.model.street.Street;

@Getter
@Setter
@EqualsAndHashCode
public class Path {

  private int distance;
  private Street street;

  public Path() {
  }

  public BigDecimal getValueTotal() {
    return street.getPricePerKm().multiply(new BigDecimal(distance));
  }
}