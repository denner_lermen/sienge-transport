package sienge.domain.model;


import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Charge {

  public static final int TONS_MIN_BEFORE_CHANGE_ADDITIONAL_VALUE = 5;
  public static final BigDecimal ADDITIONAL_VALUE_PER_TONS_TO_ADD = new BigDecimal("0.02");
  @NotNull
  private int weightInTons;

  public Charge() {
  }

  public BigDecimal getAdditionalValuePerKm() {
    if (isExceededWeight()) {
      int exceededWeight = weightInTons - TONS_MIN_BEFORE_CHANGE_ADDITIONAL_VALUE;
      return ADDITIONAL_VALUE_PER_TONS_TO_ADD.multiply(new BigDecimal(exceededWeight));
    }
    return BigDecimal.ZERO;
  }

  public boolean isExceededWeight() {
    return weightInTons > TONS_MIN_BEFORE_CHANGE_ADDITIONAL_VALUE;
  }
}
