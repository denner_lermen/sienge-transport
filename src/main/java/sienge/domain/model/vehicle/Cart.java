package sienge.domain.model.vehicle;

import java.math.BigDecimal;

public class Cart extends Vehicle {

  public static final BigDecimal FACTOR = new BigDecimal("1.12");
  public static final String NAME = "Carreta";

  public Cart() {
    super(NAME, FACTOR);
  }

}
