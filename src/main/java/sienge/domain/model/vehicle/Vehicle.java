package sienge.domain.model.vehicle;

import java.math.BigDecimal;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Vehicle {

  private String name;
  private BigDecimal factorMultiplier;

  public Vehicle() {
  }

  public Vehicle(String name, BigDecimal factorMultiplier) {
    this.name = name;
    this.factorMultiplier = factorMultiplier;
  }

}
