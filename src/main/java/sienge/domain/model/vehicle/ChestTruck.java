package sienge.domain.model.vehicle;

import java.math.BigDecimal;

public class ChestTruck extends Vehicle {

  public static final BigDecimal FACTOR = new BigDecimal("1");
  public static final String NAME = "Caminhão baú";


  public ChestTruck() {
    super(NAME, FACTOR);
  }

}
