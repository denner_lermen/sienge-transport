package sienge.domain.model.vehicle;

import java.math.BigDecimal;

public class BucketTruck extends Vehicle {

  public static final BigDecimal FACTOR = new BigDecimal("1.05");
  public static final String NAME = "Caminhão caçamba";

  public BucketTruck() {
    super(NAME, FACTOR);
  }

}
