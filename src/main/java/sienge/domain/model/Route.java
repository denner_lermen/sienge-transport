package sienge.domain.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import sienge.domain.model.vehicle.Vehicle;

@Getter
@Setter
public class Route {

  @NotNull
  private Charge charge;
  @NotNull
  private Vehicle vehicle;
  @NotNull
  private List<Path> paths;

}
