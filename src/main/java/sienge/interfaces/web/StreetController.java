package sienge.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.Collection;

import sienge.domain.model.Route;
import sienge.domain.model.street.PavedStreet;
import sienge.domain.model.street.Street;
import sienge.domain.model.street.UnpavedStreet;
import sienge.domain.service.RouteService;

@RestController
@Scope(scopeName = WebApplicationContext.SCOPE_REQUEST)
public class StreetController {

  @GetMapping("v1/streets")
  public Collection<Street> findStreets() {
    return Arrays.asList(new PavedStreet(), new UnpavedStreet());
  }

}
