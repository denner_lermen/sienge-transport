package sienge.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import sienge.domain.model.Route;
import sienge.domain.service.RouteService;

@RestController
@Scope(scopeName = WebApplicationContext.SCOPE_REQUEST)
public class RouteController {

  @Autowired
  private RouteService routeService;

  @PostMapping("v1/routes/calculate")
  public String calculateTotal(@RequestBody Route route) {
    return routeService.calculateTotal(route).toString();
  }

}
