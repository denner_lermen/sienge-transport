package sienge.interfaces.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.Collection;

import sienge.domain.model.street.PavedStreet;
import sienge.domain.model.street.Street;
import sienge.domain.model.street.UnpavedStreet;
import sienge.domain.model.vehicle.BucketTruck;
import sienge.domain.model.vehicle.Cart;
import sienge.domain.model.vehicle.ChestTruck;
import sienge.domain.model.vehicle.Vehicle;
import sienge.domain.service.RouteService;

@RestController
@Scope(scopeName = WebApplicationContext.SCOPE_REQUEST)
public class VehicleController {

  @GetMapping("v1/vehicles")
  public Collection<Vehicle> findVehicles() {
    return Arrays.asList(new Cart(), new BucketTruck(), new ChestTruck());
  }

}
